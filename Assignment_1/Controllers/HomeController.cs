﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Assignment_1.DAL;
using Assignment_1.Models;

namespace Assignment_1.Controllers
{
    public class HomeController : Controller
    {
        private HomeDAL homeContext = new HomeDAL();

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return RedirectToAction("Create", "Student");
        }
        
        public ActionResult LecturerCreation()
        {
            return RedirectToAction("LecturerCreation","Lecturer");
        }



        [HttpPost]
        public ActionResult Login(IFormCollection formData)
        {
            // Read inputs from textboxes
            // Email address converted to lowercase
            string loginID = formData["txtLoginID"].ToString();
            string password = formData["txtPassword"].ToString();
            string username;
            List<Home> homeList = homeContext.LogIn(loginID, password);
            if (homeList.Count > 0)
            {
                foreach(Home i in homeList)
                {
                    if (i.NameS != null)
                    {
                        username = i.NameS;
                        // Store Login ID in session with the key as “LoginID”
                        HttpContext.Session.SetString("LoginID", username);

                        // Store user role “Staff” in session with the key as “Role”
                        HttpContext.Session.SetString("Role", "Student");

                        // Store DateTime
                        HttpContext.Session.SetString("DateTime", DateTime.Now.ToString("dd-MMM-yyyy h:mm:ss tt"));

                        TempData.Remove("Message");

                        return RedirectToAction("Student_Dashboard", "Student");
                    }
                    else
                    {
                        username = i.NameL;
                        // Store Login ID in session with the key as “LoginID”
                        HttpContext.Session.SetString("LoginID", username);

                        // Store user role “Staff” in session with the key as “Role”
                        HttpContext.Session.SetString("Role", "Lecturer");

                        // Store DateTime
                        HttpContext.Session.SetString("DateTime", DateTime.Now.ToString("dd-MMM-yyyy h:mm:ss tt"));

                        TempData.Remove("Message");

                        return RedirectToAction("Index", "Lecturer");
                    }
                }
                return RedirectToAction();
            }
            else
            {
                TempData["Message"] = "Invalid Login Credentials!";

                // Redirect user back to the index view through an action
                return RedirectToAction("Index");
            }
        }

        public ActionResult MainPage()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();
            // Call the Index action of Home controller
            return RedirectToAction("Index");
        }
    }
}