﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Assignment_1.DAL;
using Assignment_1.Models;
using Microsoft.AspNetCore.Mvc;

namespace Assignment_1.Controllers
{
    public class LecturerController : Controller
    {
        private LecturerDAL lecturerContext = new LecturerDAL();
        private ProjectDAL projectContext = new ProjectDAL();
        private StudentDAL studentContext = new StudentDAL();
        public IActionResult Index()
        {
            return View();
        }
        // GET: Lecturer/LecturerCreation
        public ActionResult LecturerCreation()
        {
            if (HttpContext.Session.GetString("Role") == "Lecturer")
            {
                List<Student> studentList = studentContext.GetAllStudent(HttpContext.Session.GetString("LoginID"));
                return View(studentList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        // POST: Lecturer/LecturerCreation
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LecturerCreation(Lecturer lecturer)
        {

            //Add project record to database
            lecturer.LecturerID = lecturerContext.Add(lecturer);
            return RedirectToAction("Index", "Home");
        }
        public IActionResult ViewMentee()
        {
            
            if (HttpContext.Session.GetString("Role") == "Lecturer")
            {
                List<Student> studentList = studentContext.GetAllStudent(HttpContext.Session.GetString("LoginID") );
                return View(studentList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public IActionResult ChangePassword()
        {
            return View();
        }
      public IActionResult ViewSuggestions()
        {

            if (HttpContext.Session.GetString("Role") == "Lecturer")
            {
                List<Suggestion> suggestionList = lecturerContext.GetAllSuggestions(HttpContext.Session.GetString("LoginID"));
                return View(suggestionList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public IActionResult Suggestions()
        {
            if (HttpContext.Session.GetString("Role") == "Lecturer")
            {
                List<Student> studentList = studentContext.GetAllStudent(HttpContext.Session.GetString("LoginID"));
                return View(studentList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult SuggestionCreation(int? id)
        {
            // Stop accessing the action if not logged in or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (id == null) //Query string parameter not provided
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

            Suggestion suggestion = lecturerContext.GetDetails(id.Value);

            if (suggestion == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

            return View(suggestion);

        }
        /**
        // GET:  Lecturer/Post
        public ActionResult Post(int? id)
        {
            // Stop accessing the action if not logged in or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }
            Suggestion suggestion = lecturerContext.GetDetails(id.Value);
            return View(suggestion);

        }
        **/
        public ActionResult Student_Dashboard(int? id)
        {
            if (HttpContext.Session.GetString("Role") != null || HttpContext.Session.GetString("Role") == "Lecturer")
            {
                Student student = lecturerContext.GetStudents(id.Value);
                return View(student);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        
    }
}