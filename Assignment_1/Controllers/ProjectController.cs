﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Assignment_1.DAL;
using Assignment_1.Models;
using System.IO;

namespace Assignment_1.Controllers
{
    public class ProjectController : Controller
    {
        private ProjectDAL projectContext = new ProjectDAL();

        // GET: Project
        public ActionResult Index()
        {
            if ((HttpContext.Session.GetString("Role") == "Student") || (HttpContext.Session.GetString("Role") == "Lecturer"))
            {
                List<Project> projectList = projectContext.GetAllProject(HttpContext.Session.GetString("LoginID"));
                return View(projectList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            if ((HttpContext.Session.GetString("Role") == "Student") || (HttpContext.Session.GetString("Role") == "Lecturer"))
            {
                return View("Create");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: Project/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Project project)
        {
            ModelState.Remove("MemberName");
            if (ModelState.IsValid)
            {
                //Add project record to database
                project.ProjectID = projectContext.Add(project, HttpContext.Session.GetString("LoginID"));
                return RedirectToAction("Student_Dashboard", "Student");
            }
            else
            {
                //Input validation fails,return to the Create view
                //to display error message
                return View(project);
            }
        }

        // GET: Project/Details/5
        public ActionResult Details(int id)
        {
            if ((HttpContext.Session.GetString("Role") == "Student") || (HttpContext.Session.GetString("Role") == "Lecturer"))
            {
                ProjectEdit projectEdit = projectContext.EditDetails(id);
                ProjectViewModel projectVM = MapToProjectVM(projectEdit);
                return View(projectVM);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // GET: Project/Edit/5
        public ActionResult Edit(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == "Student") || (HttpContext.Session.GetString("Role") == "Lecturer"))
            {
                if (id == null) //Query string parameter not provided
                {
                    //Return to listing page, not allowed to edit
                    return RedirectToAction("Index", "Project");
                }
                ProjectEdit project = projectContext.EditDetails(id.Value);
                HttpContext.Session.SetString("project", project.ProjectID.ToString());
                return View(project);
            }

            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: Project/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProjectEdit project)
        {
            if (ModelState.IsValid)
            {
                //Update staff record to database
                projectContext.Edit(project);
                return RedirectToAction("Index", "Project");
            }

            //Input validation fails, return to the view
            //to display error message
            return View(project);
        }

        // GET: Project/Delete/5
        public ActionResult Delete(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == "Student") || (HttpContext.Session.GetString("Role") == "Lecturer"))
            {
                if (id == null)
                {
                    //Return to listing page, not allowed to edit
                    return RedirectToAction("Index", "Project");
                }

                Project project = projectContext.GetDetails(id.Value);
                return View(project);
            }

            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: Project/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Project project)
        {
            projectContext.Delete(project.ProjectID);
            return RedirectToAction("Index", "Project");
        }

        public ActionResult UploadPhoto(int id)
        {
            if ((HttpContext.Session.GetString("Role") == "Student") || (HttpContext.Session.GetString("Role") == "Lecturer"))
            {
                ProjectEdit projectEdit = projectContext.EditDetails(id);
                ProjectViewModel projectVM = MapToProjectVM(projectEdit);
                return View(projectVM);
            }

            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult AddMember(int id)
        {
            if ((HttpContext.Session.GetString("Role") == "Student") || (HttpContext.Session.GetString("Role") == "Lecturer"))
            {
                Project project = projectContext.GetDetails(id);
                project.ProjectID = id;
                return View(project);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddMember(Project project)
        {
            ModelState.Remove("Title");
            if (ModelState.IsValid)
            {
                int updated = projectContext.AddMember(project);
                if (updated == -1)
                {
                    return RedirectToAction("Index", "Project");
                }
                else
                {
                    ViewData["Member"] = "Member already exists!";
                    return View(project);
                }
            }
            else
            {
                return View(project);
            }
        }

        public ActionResult RemoveMember(int id)
        {
            if ((HttpContext.Session.GetString("Role") == "Student") || (HttpContext.Session.GetString("Role") == "Lecturer"))
            {
                projectContext.Remove(id.ToString(), Int32.Parse(HttpContext.Session.GetString("project")));
                return RedirectToAction("Edit", "Project");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadPhoto(ProjectViewModel projectVM, int id)
        {
            if (projectVM.FileToUpload != null && projectVM.FileToUpload.Length > 0)
            {
                try
                {
                    string fileExt = Path.GetExtension(projectVM.FileToUpload.FileName);

                    string uploadedFile = "Project_" + projectVM.ProjectID + "_poster" + fileExt;

                    // Get the complete path to the images folder in server
                    string savePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images\\Projects", uploadedFile);

                    // Upload the file to server
                    using (var fileStream = new FileStream(savePath, FileMode.Create))
                    {
                        await projectVM.FileToUpload.CopyToAsync(fileStream);
                    }

                    projectContext.UploadPhoto(uploadedFile, id);

                    projectVM.ProjectPoster = uploadedFile;
                    ViewData["Message"] = "File uploaded successfully.";
                }
                catch (IOException)
                {
                    //File IO error, could be due to access rights denied
                    ViewData["Message"] = "File uploading fail!";
                }
                catch (Exception ex) //Other type of error
                {
                    ViewData["Message"] = ex.Message;
                }
            }
            return View(projectVM);
        }

        public ActionResult Profile()
        {
            return RedirectToAction("Index", "Project");
        }

        public ProjectViewModel MapToProjectVM(ProjectEdit project) 
        {
            ProjectViewModel projectVM = new ProjectViewModel
            {
                ProjectID = project.ProjectID,
                Title = project.Title,
                Description = project.Description,
                ProjectPoster = project.ProjectPoster,
                ProjectURL = project.ProjectURL,
                MemberId = project.MemberId,
                MemberName = project.MemberName,
                MemberRole = project.MemberRole,
            };

            return projectVM;
        }
    }
}