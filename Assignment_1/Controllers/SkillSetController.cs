using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Assignment_1.DAL;
using Assignment_1.Models;
using System.IO;


namespace Assignment_1.Controllers
{
    public class SkillSetController : Controller
    {
        private SkillSetDAL skillSetContext = new SkillSetDAL();
        private StudentDAL studentContext = new StudentDAL();

        //GET:SkillSet/Index
        public ActionResult Index()
        {
            //Stop accessing the action if not logged in or account not in the "Lecturer" Role
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }

            //List<SkillSet> skillSetList = skillSetContext.GetDetails(HttpContext.Session.GetString("LoginID"));
            List<SkillSet> skillSetList = skillSetContext.GetAllSkillSet();
            return View(skillSetList);
        }

        // GET: SkillSet/Create
        public ActionResult Create()
        {
            //Stop accessing in the action if not logged in or account not in the "Lecturer" role
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View("Create");
        }

        // POST: SkillSet/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SkillSet skillSet)
        {
            if (ModelState.IsValid)
            {
                //Add skillset record to database
                skillSet.SkillSetID = skillSetContext.Add(skillSet);

                //Redirect user to SkillSet/Index view
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view to display error message
                return View(skillSet);
            }
        }

        //GET: SkillSet/Delete
        public ActionResult Delete(int? id)
        {
            // Stop accessing the action if not logged in// or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

            SkillSet skillSet = skillSetContext.GetDetails(id.Value);
            if (skillSet == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(skillSet);
        }

        // POST: SkillSet/Delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(SkillSet skillSet)
        {
            try
            {
                skillSetContext.Delete(skillSet.SkillSetID);
                return RedirectToAction("Index");
            }
            catch
            {
                ViewData["Message"] = "There are students using this skillset!!!";
                return View();
            }
        }

        // GET: SkillSet/Edit
        public ActionResult Edit(int? id)
        {
            // Stop accessing the action if not logged in or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (id == null) //Query string parameter not provided
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

            SkillSet skillSet = skillSetContext.GetDetails(id.Value);

            if (skillSet == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

            return View(skillSet);
            
        }

        // POST: SkillSet/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SkillSet skillSet)
        {
            if (ModelState.IsValid)
            {
                //Update SkillSet Record to database
                skillSetContext.Edit(skillSet);
                ViewData["Message"] = "SkillSet Name updated successfully";
                //return RedirectToAction("Index");
            }
            else
            {
                ViewData["Message"] = "Failed to update SkillSet Name";
            }

            //Input validation fails, return to the view to display error message
            return View(skillSet);
        }

        // GET: SkillSet/Search
        public ActionResult Search(int? id)
        {
            // Stop accessing the action if not logged in or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }

            StudentSkillSetsViewModel studentSkillVM = new StudentSkillSetsViewModel();
            studentSkillVM.skillSetList = skillSetContext.GetAllSkillSet();
            studentSkillVM.studentskillsList = skillSetContext.GetAllStudentSkills();
            
            // BranchNo (id) present in the query string
            if (id != null)
            {
                ViewData["selectedSkillID"] = id.Value;
                studentSkillVM.studentskillsList = skillSetContext.Search(id.Value);
            }
            else
            {
                ViewData["selectedSkillID"] = "";
            }
            return View(studentSkillVM);
        }

        public ActionResult StudentDetails(int? id)
        {
            if (HttpContext.Session.GetString("Role") != null || HttpContext.Session.GetString("Role") == "Lecturer")
            {
                Student student = skillSetContext.GetStudents(id.Value);
                return View(student);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

    }
}