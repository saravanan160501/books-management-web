﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Assignment_1.DAL;
using Assignment_1.Models;
using Microsoft.AspNetCore.Mvc;

namespace Assignment_1.Controllers
{
    public class StudentController : Controller
    {
        private StudentDAL studentContext = new StudentDAL();


        public IActionResult Student_Dashboard()
        {
            if (HttpContext.Session.GetString("Role") == "Student" || HttpContext.Session.GetString("Role") == "Lecturer")
            {
                List<Student> studentList = studentContext.GetStudent(HttpContext.Session.GetString("LoginID"));
                return View(studentList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View("Create");
        }


        // POST: Student/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Student student)
        {

                //Add project record to database
                student.StudentID = studentContext.Add(student);
                return RedirectToAction("Index", "Home");
        }

        // GET: Project/Edit/5
        public ActionResult Edit()
        {
            return View("Edit");
        }

        // POST: Project/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Student student)
        {
                //Update staff record to database
                studentContext.Update(student);
                return RedirectToAction("Student_Dashboard", "Student");
        }

        public IActionResult LogOut()
        {
            HttpContext.Session.SetString("Role", "");
            HttpContext.Session.SetString("LoginID", "");
            return RedirectToAction("Index", "Home");
        }
    }
}
