﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Assignment_1.Models;

namespace Assignment_1.DAL
{

    public class SuggestionDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        public SuggestionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AssignmentConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Suggestion> GetAllSuggestions(string name)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a branch record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Suggestion INNER JOIN Lecturer ON Suggestion.LecturerID=Lecturer.LecturerID WHERE Lecturer.Name= @name", conn);
            cmd.Parameters.AddWithValue("@name", name);

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the branchNo property of the Branch class. 
            /*cmd.Parameters.AddWithValue("@selectedBranch", branchNo);*/

            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            // Create a DataSet object result
            DataSet result = new DataSet();

            //Open a database connection
            conn.Open();

            //Use DataAdapter “da” to fill up a DataTable "ProjectDetails"
            //in DataSet "result".
            da.Fill(result, "SuggestionDetails");

            //Close the database connection
            conn.Close();

            List<Suggestion> SuggestionList = new List<Suggestion>();
            foreach (DataRow row in result.Tables["SuggestionDetails"].Rows)
            {
                SuggestionList.Add(
                    new Suggestion
                    {
                        SuggestionID = Convert.ToInt32(row["SuggestionID"]),
                        Description = row["Description"].ToString(),
                        Status= row["Status"].ToString(),
                        
                    }
                );
            }
            return SuggestionList;
        }
        public string Add(Suggestion suggestion)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated projectID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                ("INSERT INTO Suggestion (SuggestionID, Description,) " +
                "OUTPUT INSERTED.SuggestionID " +
                "VALUES(@suggestionID, @description,)", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            SqlParameter title = cmd.Parameters.AddWithValue("@title",suggestion.SuggestionID);
            if (suggestion.SuggestionID == null)
            {
                title.Value = DBNull.Value;
            }
            SqlParameter description = cmd.Parameters.AddWithValue("@description", suggestion.Description);
            if (suggestion.Description == null)
            {
                description.Value = DBNull.Value;
            }
           

            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //projectID after executing the INSERT SQLstatement
            suggestion.SuggestionID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.
            conn.Close();

            //Return id when no error occurs.
            return suggestion.Description;
        }

    }
}
