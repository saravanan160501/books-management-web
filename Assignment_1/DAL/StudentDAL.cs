﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Assignment_1.Models;
namespace Assignment_1.DAL
{
    public class StudentDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        //Constructor
        public StudentDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettins.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AssignmentConnectionString");

            //Instantiate a SqlConnection object with the Connection String read
            conn = new SqlConnection(strConn);
        }

        public List<Student> GetAllStudent( String id)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a branch record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student INNER JOIN Lecturer ON Student.MentorID=Lecturer.LecturerID WHERE Lecturer.Name= @id", conn);
            cmd.Parameters.AddWithValue("@id", id);

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the branchNo property of the Branch class. 
            /*cmd.Parameters.AddWithValue("@selectedBranch", branchNo);*/

            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            // Create a DataSet object result
            DataSet result = new DataSet();

            //Open a database connection
            conn.Open();

            //Use DataAdapter “da” to fill up a DataTable "ProjectDetails"
            //in DataSet "result".
            da.Fill(result, "StudentDetails");

            //Close the database connection
            conn.Close();

            List<Student> studentList = new List<Student>();
            foreach (DataRow row in result.Tables["StudentDetails"].Rows)
            {
                studentList.Add(
                    new Student
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        StudentName = row["Name"].ToString(),
                        StudentCourse = row["Course"].ToString(),
                        StudentPhoto = row["Photo"].ToString(),
                        StudentDescription = row["Description"].ToString(),
                        StudentAchievement = row["Achievement"].ToString(),
                        StudentExternalLink = row["ExternalLink"].ToString(),
                        StudentEmailAddr = row["EmailAddr"].ToString(),
                        StudentPassword = row["Password"].ToString(),
                        MentorID = Convert.ToInt32(row["MentorID"])
                    }
                );
            }
            return studentList;
        }
        public List<Student> GetStudent(string name)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a branch record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Name = @name", conn);
            cmd.Parameters.AddWithValue("@name", name);

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the branchNo property of the Branch class. 
            /*cmd.Parameters.AddWithValue("@selectedBranch", branchNo);*/

            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            // Create a DataSet object result
            DataSet result = new DataSet();

            //Open a database connection
            conn.Open();

            //Use DataAdapter “da” to fill up a DataTable "ProjectDetails"
            //in DataSet "result".
            da.Fill(result, "StudentDetails");

            //Close the database connection
            conn.Close();

            List<Student> studentList = new List<Student>();
            foreach (DataRow row in result.Tables["StudentDetails"].Rows)
            {
                studentList.Add(
                    new Student
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        StudentName = row["Name"].ToString(),
                        StudentCourse = row["Course"].ToString(),
                        StudentPhoto = row["Photo"].ToString(),
                        StudentDescription = row["Description"].ToString(),
                        StudentAchievement = row["Achievement"].ToString(),
                        StudentExternalLink = row["ExternalLink"].ToString(),
                        StudentEmailAddr = row["EmailAddr"].ToString(),
                        StudentPassword = row["Password"].ToString(),
                        MentorID = Convert.ToInt32(row["MentorID"])
                    }
                );
            }
            return studentList;
        }

        public int Add(Student student)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated projectID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                ("INSERT INTO Student (Name, Course, EmailAddr, MentorID) " +
                "OUTPUT INSERTED.StudentID " +
                "VALUES(@name, 'uk', @email, @mentorID)", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            SqlParameter name = cmd.Parameters.AddWithValue("@name", student.StudentName);
            if (student.StudentName == null)
            {
                name.Value = DBNull.Value;
            }
            SqlParameter email = cmd.Parameters.AddWithValue("@email", student.StudentEmailAddr);
            if (student.StudentEmailAddr == null)
            {
                email.Value = DBNull.Value;
            }
            SqlParameter mentorID = cmd.Parameters.AddWithValue("@mentorID", student.MentorID);
            if (student.MentorID == null)
            {
                mentorID.Value = DBNull.Value;
            }

            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //studentID after executing the INSERT SQLstatement
            student.StudentID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();

            //Return id when no error occurs.
            return student.StudentID;
        }

        public int Update(Student student)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Project SET Name=@name Course=@course, Description=@description, ExternalLink=@external, Achievement=@achievement, EmailAddr=@email, Password=@password WHERE StudentID = @selectedStudent", conn);

            cmd.Parameters.AddWithValue("@name", student.StudentName);
            cmd.Parameters.AddWithValue("@course", student.StudentCourse);
            cmd.Parameters.AddWithValue("@description", student.StudentDescription);
            cmd.Parameters.AddWithValue("@achievement", student.StudentAchievement);
            cmd.Parameters.AddWithValue("@external", student.StudentExternalLink);
            cmd.Parameters.AddWithValue("@achievement", student.StudentAchievement);
            cmd.Parameters.AddWithValue("@email", student.StudentEmailAddr);
            cmd.Parameters.AddWithValue("@password", student.StudentPassword);


            cmd.Parameters.AddWithValue("@selectedStudent", student.StudentID);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }
    }
}
