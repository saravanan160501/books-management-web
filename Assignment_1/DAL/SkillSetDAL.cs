﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Assignment_1.Models;
using Microsoft.AspNetCore.Mvc;

namespace Assignment_1.DAL
{
    public class SkillSetDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        //Constructor
        public SkillSetDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettins.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AssignmentConnectionString");

            //Instantiate a SqlConnection object with the Connection String read
            conn = new SqlConnection(strConn);
        }

        public List<SkillSet> GetAllSkillSet()
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet ORDER BY SkillSetID", conn);

            //Instantiate a DataAdapter object and pass the SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain records obtained from database
            DataSet result = new DataSet();

            //Open a database connection
            conn.Open();

            //Use DataAdapter, which execute the SELECT SQL statement through its SQLCommand object to fetch data to a 
            //table "SkillSetDetails" in DataSet "result".
            da.Fill(result, "SkillSetDetails");

            //Close the database connection
            conn.Close();

            //Transferring rows of data in DataSet's Table to "SkillSet" objects
            List<SkillSet> skillSetListL = new List<SkillSet>();
            foreach (DataRow row in result.Tables["SkillSetDetails"].Rows)
            {
                skillSetListL.Add(
                    new SkillSet
                    {
                        SkillSetID = Convert.ToInt32(row["SkillSetID"]),
                        SkillSetName = row["SkillSetName"].ToString(),
                    }
                );
            }
            return skillSetListL;
        }

        public List<StudentSkillSets> GetAllStudentSkills()
        {
            SqlCommand cmd = new SqlCommand
            ("SELECT * FROM StudentSkillSet a " +
             "INNER JOIN Student s ON a.StudentID = s.StudentID " +
             "INNER JOIN SkillSet t ON a.SkillSetID = t.SkillSetID", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "StudentSkillSetDetails");

            //Close the database connection
            conn.Close();

            //Transferring rows of data in DataSet's Table to "SkillSet" objects
            List<StudentSkillSets> studentSkillSetList = new List<StudentSkillSets>();
            foreach (DataRow row in result.Tables["StudentSkillSetDetails"].Rows)
            {
                studentSkillSetList.Add(
                    new StudentSkillSets
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        SkillSetID = Convert.ToInt32(row["SkillSetID"]),
                        //SkillSetName = row["SkillSetName"].ToString(),
                        //StudentName = row["StudentName"].ToString(),
                        //ExternalLink = row["StudentExternalLink"].ToString(),

                    }
                );
            }
            return studentSkillSetList;
        }

        public int Add(SkillSet skillSet)
        {
            // Instantiate a SqlCommand object, supply it with an INSERT SQL statement
            // which will return the auto-generated SkillSetID after insertiion,
            // and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                             ("INSERT INTO SkillSet(SkillSetName) " +
                              "OUTPUT INSERTED.SkillSetID " +
                              "VALUES(@name)", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", skillSet.SkillSetName);

            // A connection to database must be opened before any operations made.
            conn.Open();

            // ExecuteScalar is used to retrieve the uato-generated SkillSetID
            // after executing the INSERT SQL statement
            skillSet.SkillSetID = (int)cmd.ExecuteScalar();

            // A connection to database should be closed after operations.
            conn.Close();

            //return when no error occurs
            return skillSet.SkillSetID;
        }

        public bool IsNameExist(string name)
        {
            SqlCommand cmd = new SqlCommand("SELECT SkillSetID FROM SkillSet WHERE SkillSetName=@selectedName", conn);
            cmd.Parameters.AddWithValue("@selectedName", name);

            SqlDataAdapter daName = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();

            //Use DataAdapter to fetch data to a table "NameDetails" in DataSet.
            daName.Fill(result, "NameDetails");

            conn.Close();

            if (result.Tables["NameDetails"].Rows.Count > 0)
                return true; //The name exists for another skillset
            else
                return false; // The skillset name given does not exist
        }

        public SkillSet GetDetails(int skillSetId)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQLstatement which retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet WHERE SkillSetID = @selectedSkillSetID", conn);

            //Define the parameter used in SQL statement, value for the parameter is retrieved from the method parameter “skillSetId”.
            cmd.Parameters.AddWithValue("@selectedSkillSetID", skillSetId);

            //Instantiate a DataAdapter object, pass the SqlCommand object “cmd” as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object “result"
            DataSet result = new DataSet();

            //Open a database connection.
            conn.Open();

            //Use DataAdapter to fetch data to a table "SkillSetDetails" in DataSet.
            da.Fill(result, "SkillSetDetails");

            //Close the database connection
            conn.Close();

            SkillSet skillSet = new SkillSet();
            if (result.Tables["SkillSetDetails"].Rows.Count > 0)
            {
                skillSet.SkillSetID = skillSetId;

                // Fill skillset object with values from the DataSet
                DataTable table = result.Tables["SkillSetDetails"];

                if (!DBNull.Value.Equals(table.Rows[0]["SkillSetID"]))
                {
                    skillSet.SkillSetID = Convert.ToInt32(table.Rows[0]["SkillSetID"]);
                }
                if (!DBNull.Value.Equals(table.Rows[0]["SkillSetName"]))
                {
                    skillSet.SkillSetName = table.Rows[0]["SkillSetName"].ToString();
                }

                return skillSet; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }

        public int Edit(SkillSet skillSet)
        {
            //Instantiate a SqlCommand object, supply it with SQL statement UPDATE
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand("UPDATE SkillSet SET SkillSetName=@name WHERE SkillSetID = @selectedSkillSetID", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from the respective property of “staff” object.
            cmd.Parameters.AddWithValue("@name", skillSet.SkillSetName);
            cmd.Parameters.AddWithValue("@selectedSkillSetID", skillSet.SkillSetID);

            //Open a database connection.
            conn.Open();

            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();

            //Close the database connection.
            conn.Close();

            return count;
        }

        public int Delete(int skillSetId)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement 
            //to delete skillset records specified by a skillset ID 
            SqlCommand cmd = new SqlCommand("DELETE FROM SkillSet WHERE SkillSetID = @selectedSkillSetID", conn);
            cmd.Parameters.AddWithValue("@selectedSkillSetID", skillSetId);

            //Open a database connection.
            conn.Open();

            int rowCount;

            //Execute the DELETE SQL to remove the skillset record.
            rowCount = cmd.ExecuteNonQuery();

            //Close database connection.
            conn.Close();

            //Return number of row of staff record deleted.
            return rowCount;
        }

        public List<StudentSkillSets> Search(int id)
        {
            SqlCommand cmd = new SqlCommand
                ("SELECT SkillSet.SkillSetID, SkillSet.SkillSetName, StudentSkillSet.StudentID, Student.Name, Student.ExternalLink " +
                 "FROM StudentSkillSet " +
                 "INNER JOIN Student ON StudentSkillSet.StudentID = Student.StudentID " +
                 "INNER JOIN SkillSet ON StudentSkillSet.SkillSetID = SkillSet.SkillSetID " +
                 "WHERE StudentSkillSet.SkillSetID = @selectedSkillSetID", conn);

            cmd.Parameters.AddWithValue("@selectedSkillSetID", id);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "SkillSearch");

            conn.Close();

            List<StudentSkillSets> searchList = new List<StudentSkillSets>();

            foreach (DataRow row in result.Tables["SkillSearch"].Rows)
            {
                searchList.Add(
                    new StudentSkillSets
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        StudentName = row["Name"].ToString(),
                        SkillSetID = Convert.ToInt32(row["SkillSetID"]),
                        SkillSetName = row["SkillSetName"].ToString(),
                        ExternalLink = row["ExternalLink"].ToString(),
                    });
            }

            return searchList;
        }

        public Student GetStudents(int id)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQLstatement which retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE StudentID = @id", conn);

            //Define the parameter used in SQL statement, value for the parameter is retrieved from the method parameter “skillSetId”.
            cmd.Parameters.AddWithValue("@id", id);

            //Instantiate a DataAdapter object, pass the SqlCommand object “cmd” as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object “result"
            DataSet result = new DataSet();

            //Open a database connection.
            conn.Open();

            //Use DataAdapter to fetch data to a table "SkillSetDetails" in DataSet.
            da.Fill(result, "StudentDetails");

            //Close the database connection
            conn.Close();

            Student student = new Student();
            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                student.StudentID = id;

                // Fill skillset object with values from the DataSet
                DataTable table = result.Tables["StudentDetails"];

                if (!DBNull.Value.Equals(table.Rows[0]["StudentID"]))
                {
                    student.StudentID = Convert.ToInt32(table.Rows[0]["StudentID"]);
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                {
                    student.StudentName = table.Rows[0]["Name"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Course"]))
                {
                    student.StudentCourse = table.Rows[0]["Course"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                {
                    student.StudentDescription = table.Rows[0]["Description"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                {
                    student.StudentAchievement = table.Rows[0]["Achievement"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["ExternalLink"]))
                {
                    student.StudentExternalLink = table.Rows[0]["ExternalLink"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                {
                    student.StudentEmailAddr = table.Rows[0]["EmailAddr"].ToString();
                }

                return student; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }
    }
}
