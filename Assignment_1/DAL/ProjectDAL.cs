﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Assignment_1.Models;

namespace Assignment_1.DAL
{
    public class ProjectDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        //Constructor
        public ProjectDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AssignmentConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Project> GetAllProject(string name)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a project record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project INNER JOIN ProjectMember"
            + " ON Project.ProjectID = ProjectMember.ProjectID"
            + " INNER JOIN Student"
            + " ON Student.StudentID = ProjectMember.StudentID"
            + " WHERE Student.Name IN(@name)"
            + " ORDER BY Project.ProjectID", conn);
            cmd.Parameters.AddWithValue("@name", name);

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the projectNo property of the project class. 
            /*cmd.Parameters.AddWithValue("@selectedProject", projectNo);*/

            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            // Create a DataSet object result
            DataSet result = new DataSet();

            //Open a database connection
            conn.Open();

            //Use DataAdapter “da” to fill up a DataTable "ProjectDetails"
            //in DataSet "result".
            da.Fill(result, "ProjectDetails");

            //Close the database connection
            conn.Close();

            List<Project> projectList = new List<Project>();
            foreach (DataRow row in result.Tables["ProjectDetails"].Rows)
            {
                projectList.Add(
                    new Project
                    {
                        ProjectID = Convert.ToInt32(row["ProjectID"]),
                        Title = row["Title"].ToString(),
                        Description = row["Description"].ToString(),
                        ProjectPoster = row["ProjectPoster"].ToString(),
                        ProjectURL = row["ProjectURL"].ToString()
                    }
                );
            }
            return projectList;
        }

        public int Add(Project project, string name)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated projectID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                ("INSERT INTO Project (Title, Description, ProjectURL) " +
                "OUTPUT INSERTED.ProjectID " +
                "VALUES(@title, @description, @projectURL)", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            SqlParameter title = cmd.Parameters.AddWithValue("@title", project.Title);
            if (project.Title == null)
            {
                title.Value = DBNull.Value;
            }
            SqlParameter description = cmd.Parameters.AddWithValue("@description", project.Description);
            if (project.Description == null)
            {
                description.Value = DBNull.Value;
            }
            /*cmd.Parameters.AddWithValue("@projectposter", project.ProjectPoster);*/
            SqlParameter projectURL = cmd.Parameters.AddWithValue("@projectURL", project.ProjectURL);
            if (project.ProjectURL == null)
            {
                projectURL.Value = DBNull.Value;
            }

            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //projectID after executing the INSERT SQLstatement
            project.ProjectID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();

            SqlCommand cmd2 = new SqlCommand("INSERT INTO ProjectMember(ProjectID, StudentID, Role) " +
                "VALUES(@ProjectID, (SELECT StudentID FROM Student WHERE Name = @Name), 'Leader')", conn);
            cmd2.Parameters.AddWithValue("@Name", name);
            cmd2.Parameters.AddWithValue("@ProjectID", project.ProjectID);

            conn.Open();

            cmd2.ExecuteScalar();

            conn.Close();

            //Return id when no error occurs.
            return project.ProjectID;
        }
    
        public int Update(Project project)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Project SET Title=@title, Description=@description, ProjectURL=@projectURL WHERE ProjectID = @selectedProjectID", conn);

            cmd.Parameters.AddWithValue("@title", project.Title);
            cmd.Parameters.AddWithValue("@description", project.Description);
            cmd.Parameters.AddWithValue("@projectURL", project.ProjectURL);
            cmd.Parameters.AddWithValue("@selectedProjectID", project.ProjectID);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }

        public int Edit(ProjectEdit project)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Project SET Title=@title, Description=@description, ProjectURL=@projectURL WHERE ProjectID = @selectedProjectID", conn);

            cmd.Parameters.AddWithValue("@title", project.Title);
            cmd.Parameters.AddWithValue("@description", project.Description);
            cmd.Parameters.AddWithValue("@projectURL", project.ProjectURL);
            cmd.Parameters.AddWithValue("@selectedProjectID", project.ProjectID);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }

        public int Delete(int projectID)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "DELETE FROM ProjectMember WHERE ProjectID = @selectedProjectID;"
                + "DELETE FROM Project WHERE ProjectID = @selectedProjectID";

            cmd.Parameters.AddWithValue("@selectedProjectID", projectID);

            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }

        public bool IsProjectExist(string project)
        {
            SqlCommand cmd = new SqlCommand("SELECT ProjectID FROM Project WHERE Title = @selectedProject", conn);
            cmd.Parameters.AddWithValue("@selectedProject", project);
            SqlDataAdapter daProject = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daProject.Fill(result, "ProjectDetails");
            conn.Close();
            if (result.Tables["ProjectDetails"].Rows.Count > 0)
                return true;
            else
                return false;
        }

        public Project GetDetails(int projectId)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE ProjectID = @selectedProjectID", conn);

            cmd.Parameters.AddWithValue("@selectedProjectID", projectId);

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "ProjectDetails");

            conn.Close();

            Project project = new Project();
            if (result.Tables["ProjectDetails"].Rows.Count > 0)
            {
                project.ProjectID = projectId;
                DataTable table = result.Tables["ProjectDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectID"])) project.ProjectID = Convert.ToInt32(table.Rows[0]["ProjectID"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Title"])) project.Title = table.Rows[0]["Title"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Description"])) project.Description = table.Rows[0]["Description"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectPoster"])) project.ProjectPoster = table.Rows[0]["ProjectPoster"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectURL"])) project.ProjectURL = table.Rows[0]["ProjectURL"].ToString();

                return project; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }

        public ProjectEdit EditDetails(int projectId)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project INNER JOIN ProjectMember"
            + " ON Project.ProjectID = ProjectMember.ProjectID"
            + " INNER JOIN Student"
            + " ON Student.StudentID = ProjectMember.StudentID"
            + " WHERE Project.ProjectID = @projectID", conn);

            cmd.Parameters.AddWithValue("@projectID", projectId);

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "ProjectDetails");

            conn.Close();

            ProjectEdit project = new ProjectEdit();
            if (result.Tables["ProjectDetails"].Rows.Count > 0)
            {
                project.ProjectID = projectId;
                DataTable table = result.Tables["ProjectDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectID"])) project.ProjectID = Convert.ToInt32(table.Rows[0]["ProjectID"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Title"])) project.Title = table.Rows[0]["Title"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Description"])) project.Description = table.Rows[0]["Description"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectPoster"])) project.ProjectPoster = table.Rows[0]["ProjectPoster"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectURL"])) project.ProjectURL = table.Rows[0]["ProjectURL"].ToString();

                List<ProjectEdit> memberList = new List<ProjectEdit>();
                foreach (DataRow row in result.Tables["ProjectDetails"].Rows)
                {
                    memberList.Add(
                        new ProjectEdit
                        {
                            MemberName = row["Name"].ToString(),
                            MemberRole = row["Role"].ToString(),
                            MemberId = row["StudentID"].ToString(),
                        }
                    );
                }
                project.memberList = memberList.OrderBy(m => m.MemberRole).ToList();

                return project; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }

        public ProjectViewModel VMDetails(int projectId)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project INNER JOIN ProjectMember"
            + " ON Project.ProjectID = ProjectMember.ProjectID"
            + " INNER JOIN Student"
            + " ON Student.StudentID = ProjectMember.StudentID"
            + " WHERE Project.ProjectID = @projectID", conn);

            cmd.Parameters.AddWithValue("@projectID", projectId);

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "ProjectDetails");

            conn.Close();

            ProjectViewModel project = new ProjectViewModel();
            if (result.Tables["ProjectDetails"].Rows.Count > 0)
            {
                project.ProjectID = projectId;
                DataTable table = result.Tables["ProjectDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectID"])) project.ProjectID = Convert.ToInt32(table.Rows[0]["ProjectID"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Title"])) project.Title = table.Rows[0]["Title"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Description"])) project.Description = table.Rows[0]["Description"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectPoster"])) project.ProjectPoster = table.Rows[0]["ProjectPoster"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ProjectURL"])) project.ProjectURL = table.Rows[0]["ProjectURL"].ToString();

                List<ProjectViewModel> memberList = new List<ProjectViewModel>();
                foreach (DataRow row in result.Tables["ProjectDetails"].Rows)
                {
                    memberList.Add(
                        new ProjectViewModel
                        {
                            MemberName = row["Name"].ToString(),
                            MemberRole = row["Role"].ToString(),
                            MemberId = row["StudentID"].ToString(),
                        }
                    );
                }
                project.memberList = memberList.OrderBy(m => m.MemberRole).ToList();

                return project; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }

        public int UploadPhoto(string uploadedFile, int projectId)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Project SET ProjectPoster = @projectPoster WHERE ProjectID = @selectedProjectID", conn);

            cmd.Parameters.AddWithValue("@selectedProjectID", projectId);
            cmd.Parameters.AddWithValue("@projectPoster", uploadedFile);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return count;
        }

        public ProjectViewModel Remove(string memberId, int projectId)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM ProjectMember WHERE StudentID = @memberId AND Role <> 'Leader' AND ProjectID = @projectId", conn);

            cmd.Parameters.AddWithValue("@memberId", memberId);
            cmd.Parameters.AddWithValue("@projectId", projectId);

            conn.Open();

            int count = cmd.ExecuteNonQuery();

            conn.Close();

            return null;
        }

        public bool IsMember(string name)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Name = @memberName", conn);
            cmd.Parameters.AddWithValue("@memberName", name);
            SqlDataAdapter daProject = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daProject.Fill(result, "MemberDetails");
            conn.Close();
            if (result.Tables["MemberDetails"].Rows.Count > 0)
                return true;
            else
                return false;
        }

        public int AddMember(Project project)
        {
            int count = -1;
            SqlCommand check = new SqlCommand("SELECT * FROM ProjectMember INNER JOIN Student ON ProjectMember.StudentID = Student.StudentID WHERE Name = @memberName AND ProjectID = @projectId", conn);
            check.Parameters.AddWithValue("@memberName", project.MemberName);
            check.Parameters.AddWithValue("@projectId", project.ProjectID);

            conn.Open();
            try {
                count = (int)check.ExecuteScalar();
            }
            catch
            {
                check.ExecuteNonQuery();
            }
            conn.Close();

            if (count == -1)
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO ProjectMember(ProjectID, StudentID, Role)" +
                "VALUES (@projectId, (SELECT StudentID FROM Student WHERE Name = @memberName), 'Member')", conn);
                cmd.Parameters.AddWithValue("@memberName", project.MemberName);
                cmd.Parameters.AddWithValue("@projectId", project.ProjectID);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            return count;
        }
    }
}
