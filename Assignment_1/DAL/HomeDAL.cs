﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Assignment_1.Models;

namespace Assignment_1.DAL
{
    public class HomeDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        public HomeDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AssignmentConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Home> LogIn(string email, string password)
        {
            SqlCommand cmds = new SqlCommand("SELECT Name FROM Student WHERE EmailAddr = @email AND Password = @password", conn);
            SqlCommand cmdl = new SqlCommand("SELECT Name FROM Lecturer WHERE EmailAddr = @email AND Password = @password", conn);

            cmds.Parameters.AddWithValue("@email", email);
            cmds.Parameters.AddWithValue("@password", password);
            cmdl.Parameters.AddWithValue("@email", email);
            cmdl.Parameters.AddWithValue("@password", password);

            SqlDataAdapter das = new SqlDataAdapter(cmds);
            SqlDataAdapter dal = new SqlDataAdapter(cmdl);

            DataSet result = new DataSet();

            conn.Open();

            das.Fill(result, "StudentName");
            dal.Fill(result, "LecturerName");

            conn.Close();

            List<Home> homeList = new List<Home>();
            foreach (DataRow row in result.Tables["StudentName"].Rows)
            {
                homeList.Add(
                    new Home
                    {
                        NameS = row["Name"].ToString(),
                    }
                );
            }

            foreach (DataRow row in result.Tables["LecturerName"].Rows)
            {
                homeList.Add(
                    new Home
                    {
                        NameL = row["Name"].ToString(),
                    }
                );
            }

            return homeList;
        }
    }
}
