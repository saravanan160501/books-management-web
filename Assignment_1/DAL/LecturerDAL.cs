﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Assignment_1.Models;

namespace Assignment_1.DAL
{
    public class LecturerDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;

        //Constructor
        public LecturerDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettins.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AssignmentConnectionString");

            //Instantiate a SqlConnection object with the Connection String read
            conn = new SqlConnection(strConn);
        }

        public List<Lecturer> GetAllLecturer()
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Lecturer ORDER BY LecturerID", conn);

            //Instantiate a DataAdapter object and pass the SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain records obtained from database
            DataSet result = new DataSet();

            //Open a database connection
            conn.Open();

            //Use DataAdapter, which execute the SELECT SQL statement through its SQLCommand object to fetch data to a 
            //table "LecturerDetails" in DataSet "result".
            da.Fill(result, "LecturerDetails");

            //Close the database connection
            conn.Close();

            //Transferring rows of data in DataSet's Table to "Lecturer" objects
            List<Lecturer> lecturerList = new List<Lecturer>();
            foreach (DataRow row in result.Tables["LecturerDetails"].Rows)
            {
                lecturerList.Add(
                    new Lecturer
                    {
                        LecturerID = Convert.ToInt32(row["LecturerID"]),
                        LecturerName = row["LecturerName"].ToString(),
                    }
                );
            }
            return lecturerList;
        }

        public int Add(Lecturer lecturer)
        {
            // Instantiate a SqlCommand object, supply it with an INSERT SQL statement
            // which will return the auto-generated SkillSetID after insertiion,
            // and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                             ("INSERT INTO Lecturer (Name , EmailAddr , Description , Password) " +
                              "OUTPUT INSERTED.LecturerID " +
                              "VALUES(@name, @emailaddr , @description, @password)", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", lecturer.LecturerName);
            cmd.Parameters.AddWithValue("@emailaddr", lecturer.EmailAddr);
            cmd.Parameters.AddWithValue("@description", lecturer.Description);
            cmd.Parameters.AddWithValue("@password", "p@55Lecturer");

            // A connection to database must be opened before any operations made.
            conn.Open();

            // ExecuteScalar is used to retrieve the ato-generated LecturerID
            // after executing the INSERT SQL statement
            lecturer.LecturerID = (int)cmd.ExecuteScalar();

            // A connection to database should be closed after operations.
            conn.Close();

            //return when no error occurs
            return lecturer.LecturerID;
        }
        public bool IsLecturerExist(string name)
        {
            SqlCommand cmd = new SqlCommand("SELECT LecturerID FROM Lecturer WHERE LecturerName=@selectedName", conn);
            cmd.Parameters.AddWithValue("@selectedName", name);

            SqlDataAdapter daName = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();

            //Use DataAdapter to fetch data to a table "NameDetails" in DataSet.
            daName.Fill(result, "NameDetails");

            conn.Close();

            if (result.Tables["NameDetails"].Rows.Count > 0)
                return true; //The name exists for another skillset
            else
                return false; // The skillset name given does not exist
        }
        public Suggestion GetDetails(int suggestionid)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQLstatement which retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Suggestion WHERE SuggestionID = @selectedsuggestionid", conn);

            //Define the parameter used in SQL statement, value for the parameter is retrieved from the method parameter “skillSetId”.
            cmd.Parameters.AddWithValue("@selectedsuggestionid", suggestionid);

            //Instantiate a DataAdapter object, pass the SqlCommand object “cmd” as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object “result"
            DataSet result = new DataSet();

            //Open a database connection.
            conn.Open();

            //Use DataAdapter to fetch data to a table "SkillSetDetails" in DataSet.
            da.Fill(result, "SuggestionDetails");

            //Close the database connection
            conn.Close();

            Suggestion suggestion = new Suggestion();
            if (result.Tables["SuggestionDetails"].Rows.Count > 0)
            {
                suggestion.SuggestionID = suggestionid;

                // Fill skillset object with values from the DataSet
                DataTable table = result.Tables["SuggestionDetails"];

                if (!DBNull.Value.Equals(table.Rows[0]["SuggestionID"]))
                {
                    suggestion.SuggestionID = Convert.ToInt32(table.Rows[0]["SuggestionID"]);
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                {
                    suggestion.Description = table.Rows[0]["Description"].ToString();
                }

                return suggestion; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }

        public List<Suggestion> GetAllSuggestions(string name)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a branch record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Suggestion INNER JOIN Lecturer ON Suggestion.LecturerID=Lecturer.LecturerID WHERE Lecturer.Name= @name", conn);
            cmd.Parameters.AddWithValue("@name", name);

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the branchNo property of the Branch class. 
            /*cmd.Parameters.AddWithValue("@selectedBranch", branchNo);*/

            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            // Create a DataSet object result
            DataSet result = new DataSet();

            //Open a database connection
            conn.Open();

            //Use DataAdapter “da” to fill up a DataTable "ProjectDetails"
            //in DataSet "result".
            da.Fill(result, "SuggestionDetails");

            //Close the database connection
            conn.Close();

            List<Suggestion> SuggestionList = new List<Suggestion>();
            foreach (DataRow row in result.Tables["SuggestionDetails"].Rows)
            {
                SuggestionList.Add(
                    new Suggestion
                    {
                        SuggestionID = Convert.ToInt32(row["SuggestionID"]),
                        Description = row["Description"].ToString(),
                        Status = row["Status"].ToString(),

                    }
                );
            }
            return SuggestionList;
        }
        public Student GetStudents(int id)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQLstatement which retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE StudentID = @id", conn);

            //Define the parameter used in SQL statement, value for the parameter is retrieved from the method parameter “skillSetId”.
            cmd.Parameters.AddWithValue("@id", id);

            //Instantiate a DataAdapter object, pass the SqlCommand object “cmd” as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //Create a DataSet object “result"
            DataSet result = new DataSet();

            //Open a database connection.
            conn.Open();

            //Use DataAdapter to fetch data to a table "SkillSetDetails" in DataSet.
            da.Fill(result, "StudentDetails");

            //Close the database connection
            conn.Close();

            Student student = new Student();
            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                student.StudentID = id;

                // Fill skillset object with values from the DataSet
                DataTable table = result.Tables["StudentDetails"];

                if (!DBNull.Value.Equals(table.Rows[0]["StudentID"]))
                {
                    student.StudentID = Convert.ToInt32(table.Rows[0]["StudentID"]);
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                {
                    student.StudentName = table.Rows[0]["Name"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Course"]))
                {
                    student.StudentCourse = table.Rows[0]["Course"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                {
                    student.StudentDescription = table.Rows[0]["Description"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                {
                    student.StudentAchievement = table.Rows[0]["Achievement"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["ExternalLink"]))
                {
                    student.StudentExternalLink = table.Rows[0]["ExternalLink"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                {
                    student.StudentEmailAddr = table.Rows[0]["EmailAddr"].ToString();
                }

                return student; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }
    }
    

   
}
