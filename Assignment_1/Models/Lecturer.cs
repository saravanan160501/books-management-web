﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Assignment_1.Models
{
    public class Lecturer
    {
        [Key]
        [Display(Name ="ID:")]
        public int LecturerID { get; set; }

        [Required(ErrorMessage ="Name is required")]
        [Display(Name ="Name:")]
        [StringLength(50,ErrorMessage ="Must not exceed 50 characters")]
        public string LecturerName { get; set; }

        [Required(ErrorMessage ="Email is required")]
        [Display(Name="Email Address:")]
        [EmailAddress(ErrorMessage ="Invalid email")]
        public string EmailAddr { get; set; }

        [Required(ErrorMessage ="Password is required")]
        [StringLength(255,ErrorMessage ="Must not exceed 255 characters")]
        [Display(Name ="Password:")]
        public string Password { get; set; }

        [Display(Name ="Description:")]
        [StringLength(3000,ErrorMessage ="Must not exceed 3000 characters")]
        public string Description { get; set; }
            

    }
}
