﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Assignment_1.Models
{
    public class Project
    {
        public int ProjectID { get; set; }

        [Required]
        [Display(Name="Title")]
        [ValidateProjectExists(ErrorMessage = "Project already exists!")]
        public string Title { get; set; }

        [Display(Name="Description")]
        public string Description { get; set; }

        [Display(Name="Project Poster")]
        public string ProjectPoster { get; set; }

        [Display(Name = "Project URL")]
        [RegularExpression(@"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$", ErrorMessage = "Hmm, that does not look like an URL.")]
        public string ProjectURL { get; set; }

        [Display(Name = "Member Name")]
        [ValidateMember(ErrorMessage = "Member does not exist!")]
        public string MemberName { get; set; }
    }
}
