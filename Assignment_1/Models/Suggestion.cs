﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Assignment_1.Models
{
    public class Suggestion
    {
        public int SuggestionID { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        public String Status { get; set; }
        
        public DateTime DateCreated { get; set; } 
    }
}
