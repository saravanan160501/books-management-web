﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Assignment_1.Models
{
    public class SkillSet
    {
        // SkillSet Table
        [Key]
        [Display(Name = "ID:")]
        public int SkillSetID { get; set; }

        [Display(Name = "Name:")]
        [Required(ErrorMessage = "Please Fill This Up!")]
        [StringLength(255, ErrorMessage = "Name Cannot Exceed 255 Characters!")]
        //Custom Validation Attribute for checking skillset name exists
        [ValidateSkillSetExists(ErrorMessage = "This name already exists!")]
        public string SkillSetName { get; set; }

        public List<StudentSkillSetsViewModel> StudentSkillSets{ get; set; }
    }
}
