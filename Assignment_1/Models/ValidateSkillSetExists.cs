﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Assignment_1.DAL;

namespace Assignment_1.Models
{
    public class ValidateSkillSetExists : ValidationAttribute
    {
        private SkillSetDAL skillSetContext = new SkillSetDAL();

        public override bool IsValid(object value)
        {
            string name = Convert.ToString(value);

            if (skillSetContext.IsNameExist(name))
            {
                return false; // validation failed
            }
            else
            {
                return true; // validation passed
            }
        }
    }
}
