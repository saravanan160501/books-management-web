﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Assignment_1.Models
{
    public class ProjectEdit
    {
        public int ProjectID { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Project Poster")]
        public string ProjectPoster { get; set; }

        [Display(Name = "Project URL")]
        [RegularExpression(@"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$", ErrorMessage = "Hmm, that does not look like an URL.")]
        public string ProjectURL { get; set; }

        public string MemberName { get; set; }

        public string MemberRole { get; set; }

        public string MemberId { get; set; }

        [Display(Name = "Members")]
        public List<ProjectEdit> memberList { get; set; }
    }
}
