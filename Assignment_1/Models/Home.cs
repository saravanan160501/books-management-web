﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_1.Models
{
    public class Home
    {
        public string NameS { get; set; }

        public string NameL { get; set; }

        public string EmailS { get; set; }

        public string EmailL { get; set; }

        public string PasswordS { get; set; }

        public string PasswordL { get; set; }
    }
}
