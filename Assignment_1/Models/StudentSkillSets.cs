﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Assignment_1.Models
{
    public class StudentSkillSets
    {
        [Display(Name = "StudentID")]
        public int StudentID { get; set; }

        [Display(Name = "StudentName")]
        public string StudentName { get; set; }

        [Display(Name = "SkillID:")]
        public int SkillSetID { get; set; }

        [Display(Name = "SkillName:")]
        [Required(ErrorMessage = "Please Fill This Up!")]
        [StringLength(255, ErrorMessage = "Name Cannot Exceed 255 Characters!")]
        //Custom Validation Attribute for checking skillset name exists
        [ValidateSkillSetExists(ErrorMessage = "This name already exists!")]
        public string SkillSetName { get; set; }

        [Display(Name = "External Link:")]
        [StringLength(255, ErrorMessage = "URL Cannot Exceed 255 Characters!")]
        public string ExternalLink { get; set; }
    }
}
