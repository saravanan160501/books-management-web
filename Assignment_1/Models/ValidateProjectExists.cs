﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Assignment_1.DAL;

namespace Assignment_1.Models
{
    public class ValidateProjectExists : ValidationAttribute
    {
        private ProjectDAL projectContext = new ProjectDAL();

        public override bool IsValid(object value)
        {
            string title = Convert.ToString(value);
            if (projectContext.IsProjectExist(title))
                return false;
            else
                return true;
        }
    }
}
