﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
namespace Assignment_1.Models
{
    public class Student
    {
        [Key]
        [Display(Name ="ID:")]
        public int StudentID { get; set; }


        [Display(Name ="Name:")]
        [Required(ErrorMessage = "Please Fill This Up!")]
        [StringLength(50, ErrorMessage = "Name Cannot Exceed 50 Characters!")]
        public string StudentName { get; set; }

        [Display(Name = "Course:")]
        [Required(ErrorMessage = "Please Fill This Up!")]
        [StringLength(50, ErrorMessage = "Course Cannot Exceed 50 Characters!")]
        public string StudentCourse { get; set; }

        [Display(Name = "Photo:")]
        [StringLength(255, ErrorMessage = "Filename Cannot Exceed 255 Characters!")]
        public string StudentPhoto { get; set; }

        [Display(Name = "Description:")]
        [StringLength(3000, ErrorMessage = "Description Cannot Exceed 3000 Characters!")]
        public string StudentDescription { get; set; }

        [Display(Name = "Achievement:")]
        [StringLength(3000, ErrorMessage = "Achievement Cannot Exceed 3000 Characters!")]
        public string StudentAchievement { get; set; }

        [Display(Name = "External Link:")]
        [StringLength(255, ErrorMessage = "URL Cannot Exceed 255 Characters!")]
        public string StudentExternalLink { get; set; }

        [Display(Name="Email Address:")]
        [Required(ErrorMessage = "Please Fill This Up!")]
        [StringLength(50, ErrorMessage = "Email Address Cannot Exceed 50 Characters!")]
        public string StudentEmailAddr { get; set; }

        [Display(Name ="Password:")]
        [Required(ErrorMessage = "Please Fill This Up!")]
        [StringLength(255, ErrorMessage = "Password Cannot Exceed 255 Characters!")]
        public string StudentPassword { get; set; }

        [Display(Name ="MentorID:")]
        [Required(ErrorMessage = "Please Fill This Up!")]
        public int MentorID { get; set; }
            

    }
}
