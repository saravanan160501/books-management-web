﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Assignment_1.Models
{
    public class StudentSkillSetsViewModel
    {
        public List<SkillSet> skillSetList { get; set; }
        public List<Student> studentList { get; set; }
        public List<StudentSkillSets> studentskillsList { get; set; }

    }
}
