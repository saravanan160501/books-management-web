/* DatabaseSetup.sql           */
/* Creation Date 16/05/2019     */

IF DB_ID('Assignment') IS NULL
	CREATE DATABASE Assignment
GO

use Assignment

/*** Deletion of tables ***/

/* Table: dbo.Project */
if exists (select * from sysobjects 
  where id = object_id('dbo.Project') and sysstat & 0xf = 3)
  drop table dbo.Project
GO

/* Table: dbo.ProjectMember */
if exists (select * from sysobjects 
  where id = object_id('dbo.ProjectMember') and sysstat & 0xf = 3)
  drop table dbo.ProjectMember
GO

/* Table: dbo.SkillSet */
if exists (select * from sysobjects 
  where id = object_id('dbo.SkillSet') and sysstat & 0xf = 3)
  drop table dbo.SkillSet
GO

/* Table: dbo.StudentSkillSet */
if exists (select * from sysobjects 
  where id = object_id('dbo.StudentSkillSet') and sysstat & 0xf = 3)
  drop table dbo.StudentSkillSet
GO


/*** Create tables ***/

/* Table: dbo.Project */
CREATE TABLE dbo.Project 
(
  ProjectID int IDENTITY NOT NULL,
  Title varchar(255) NOT NULL,
  Description varchar(3000) NULL,
  ProjectPoster varchar(255) NULL,
  ProjectURL varchar(255) NULL,
  CONSTRAINT PK_Project PRIMARY KEY (ProjectID),
)
GO

/* Table: dbo.ProjectMember */
CREATE TABLE dbo.ProjectMember
(
	ProjectID int NOT NULL,
	StudentID int NOT NULL,
	Role varchar(50) NOT NULL,
	CONSTRAINT FK_ProjectID FOREIGN KEY (ProjectID) REFERENCES dbo.Project(ProjectId),
/*	CONSTRAINT FK_StudentID FOREIGN KEY (StudentID) REFERENCES dbo.Project(StudentId), */
)
GO

/*Table: dbo.SkillSet*/
CREATE TABLE dbo.SkillSet
(
	SkillSetID int NOT NULL,
	SkillSetName varchar(255) NOT NULL,
	CONSTRAINT PK_SkillSetID PRIMARY KEY (SkillSetID),
)
GO

/*Table: dbo.StudentSkillSet*/
CREATE TABLE dbo.StudentSkillSet
(
	StudentID int NOT NULL,
	SkillSetID int NOT NULL,
	CONSTRAINT FK_SkillSetID FOREIGN KEY (SkillSetID) REFERENCES dbo.SkillSet(SkillSetID),
	/*CONSTRAINT FK_StudentID FOREIGN KEY (StudentID) REFERENCES dbo.Project(StudentId),*/
)
GO
/*///////////////////////////////////////////////////////////
Lecturer Database
*/
if DB_ID('Lecturer') is null
create database Lecturer
go

use Lecturer

if exists (select * from sysobjects 
  where id = object_id('dbo.LecturerID') and sysstat & 0xf = 3)
  drop table dbo.LecturerID
GO
if exists (select * from sysobjects 
  where id = object_id('dbo.Project') and sysstat & 0xf = 3)
  drop table dbo.Project
GO

/* Insert rows */